/* FUNCTIONS */ 

let sumOfListElements = (list_asdfasdf) => {
      let sum = 0; 
      list_asdfasdf.forEach(element => sum += element); 
      return sum; 
}

let singleLine = (a, b) => a + b; 

function sumOfListElementsPrevious(list_of_elements) {
      let sum = 0; 
      for (let i = 0; i < list_of_elements.length; i++) {
            sum += list_of_elements[i]; 
      }
      return sum;
}

let list = [1,2,3,4,5,6,7]; 
console.log(sumOfListElementsPrevious(list)); 

/* FACTORIAL of N */ 
let factorial = N => {
      let product = 1; 
      for (let i = 1; i <= N; i++) {
            product *= i; 
      }
      return product; 
}

for (let i = 1; i <= 10; i++) {
      console.log("FACTORIAL OF : ", i, " IS : ", factorial(i)); 
}