let integer_number = 10; 
let floating_number = 10.123123; 
let name = "ABHISHEK"; 
let another_name = 'TOTN'; 
let boolean_false = false; 
let boolean_true = true; 

let person = {
      name: 'ABHISHEK', 
      age: 22, 
      subjects: {
            maths: 98.8, 
            physics: 100, 
            chemistry: 78.5
      }
}; 

console.log("PERSON NAME :", person.name); 
console.log("PERSON NAME : " + person.name); 
console.log("PERSON AGE : ", person.age); 
let average = (person.subjects.maths + person.subjects.physics + 
      person.subjects.chemistry) / 3; 
console.log("AVERAGE IS : ", average);  