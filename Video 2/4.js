let first = 10; 
let second = 20; 
let first_string = "10"; 

console.log("== Check => ", (first == first_string)); 
console.log("=== Check => ", (first === first_string)); 

let authorised = true; 
let password_correct = false; 

let grantAccess = authorised && password_correct; 
console.log("GRANT ACCESS? ", grantAccess); 

let orCheck = authorised || password_correct; 
console.log(orCheck); 